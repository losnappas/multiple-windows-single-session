module Main where

import Prelude (Unit, bind, discard, flip, map, pure, unit, void, ($), (<>), (=<<), (==))
import Effect (Effect)
import Effect.Console (log)
import Browser.BrowserAction (onClicked) as BrowserAction
import Browser.Event (eventListener, EventListener)
import Browser.Windows as W
import Browser.Storage.Local as SL
import Browser.Sessions as S
import Browser.Runtime as R
import Data.Either (either)
import Effect.Exception (Error, message, stack)
import Data.Array (cons, drop, filter, head, null)
import Data.Traversable (any, traverse_)
import Data.Maybe (Maybe(..), fromMaybe, maybe)
import Data.Options ((:=))
import Effect.AVar as AV
import Effect.Promise (class Deferred, Promise, resolve, runPromise)
import Data.String as Str


main :: Effect Unit
main = do
	av <- AV.empty
	BrowserAction.onClicked =<< (closeHandler av)
	R.onStartup =<< restoreWindows
	W.onRemoved =<< (removedHandler av)
	-- onSuspend does not exist in Firefox at the time (62).
	-- Doing it in onStartup instead, before anything else.
--	R.onSuspend =<< ignoreLastWindow

-- | Remove latest closed window's URL's from storage.
--ignoreLastWindow :: Effect EventListener
--ignoreLastWindow = eventListener $ \event -> do
--	runPromise pure onError cutLast

cutLast :: Deferred => Promise Unit
cutLast = do
	p <- getURLsFromStorage
	maybe (pure unit) (\xs -> setStorage (drop 1 xs)) p

-- | Launches windows from URLs stored in local storage.
restoreWindows :: Effect EventListener
restoreWindows = eventListener $ \event -> do
	runPromise pure onError openWindows
	runPromise pure onError (SL.clear unit)
	
openWindows :: Deferred => Promise Unit
openWindows = do
--	cutLast -- Apparently, for reasons unknown, last tab doesn't get saved.
	windowsURLs <- getURLsFromStorage
	maybe (pure unit) (traverse_ openWindow) windowsURLs
	
openWindow :: Deferred => Array String -> Promise W.Window
openWindow urls =
	W.create $ W.url := urls

getURLsFromStorage :: Deferred => Promise (Maybe (Array (Array String)))
getURLsFromStorage = do
	urls <- SL.get $ Just { windowURLs: [] }
	resolve if null urls.windowURLs
		then Nothing
		else Just urls.windowURLs

-- | When a window is closed(removed) its tabs' URLs are stored.
removedHandler :: AV.AVar String ->  Effect EventListener
removedHandler av = eventListener $ \event -> do
	res <- AV.tryTake av
	-- TODO: could this be changed into `pure $ saveTabs`..etc.?
	-- It cries about 'Deferred'.
	case res of
		Nothing -> runPromise pure onError saveTabs
		Just s -> pure unit

saveTabs :: Deferred => Promise Unit
saveTabs = do
	sessions <- S.getRecentlyClosed $ S.maxResults := 1
	setURLsToStorage $ head sessions

setURLsToStorage :: Deferred => Maybe S.Session -> Promise Unit
setURLsToStorage Nothing = pure unit
setURLsToStorage (Just session) = do
	oldURLsM <- getURLsFromStorage
	let newURLs = map (\tab -> tab.url) session.window.tabs
	    allURLs = maybe [newURLs] (cons newURLs) oldURLsM
	setStorage allURLs

-- | For storing. In this case an Array of Array of URLs.
setStorage :: Deferred => Array (Array String) -> Promise Unit
setStorage v = do
	-- TODO: Maybe indexOf == 0 is faster. It's whatever here.
	let validURLstarts = ["https://","http://","ftp://"]
	    myFilter = filter (\url -> any ((flip startsWith) url) validURLstarts)
	    qualified = map myFilter v
	SL.set { windowURLs: qualified }

-- | On closing a window without storing the URLs so it doesn't get restored.
closeHandler :: AV.AVar String ->  Effect EventListener
closeHandler av = eventListener $ \event -> 
	void $ AV.put "close" av $ either onError \_ -> 
		runPromise pure onError (removeWindow event.windowId)
		

removeWindow :: Deferred => Int -> Promise Unit
removeWindow id = W.remove id

-- Utility/useless
onError :: Error -> Effect Unit
onError e
  = log $ "Uh oh, an error happened! " <>
    message e <>
    "\nStack: " <>
    fromMaybe "No stack." (stack e)

-- | String to check -> Target -> Boolean (e.g. "123" "12345" == true).
-- TODO: should've used lists... They got Cons & are better for this job.
startsWith :: String -> String -> Boolean
startsWith _ "" = false
startsWith "" _ = true
startsWith xs ys = case Str.uncons xs of
	Just { head: x, tail: xss } -> case Str.uncons ys of
		Just { head: y, tail: yss } -> if x==y then startsWith xss yss else false
		Nothing -> false
	Nothing -> false


--startsWith (Cons x xs) (Cons y ys) 
--	| x == y = startsWith xs ys
--	| otherwise = false
--
